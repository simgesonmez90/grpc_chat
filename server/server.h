#include <memory>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <thread>
#include "chat.grpc.pb.h"
#include <google/protobuf/util/time_util.h>
#include "grpcpp/server.h"
#include "grpcpp/server_builder.h"

namespace cross {
class Server {
public:
    Server();
    ~Server() = default;

    void Run();
    void ShutDown();

    std::unique_ptr<grpc::Server> _server;
private:
    class ServiceImpl;
    std::unique_ptr<ServiceImpl> _service;
};
}