#include "server.h"

namespace cross {
void Read(grpc::ServerReaderWriter<chat::Message, chat::Message>* stream) {
    cross::chat::Message message;
    while (stream->Read(&message)) {
        std::string full_text;
        google::protobuf::Descriptor const *desc = message.descriptor();
        std::for_each(message.text().begin(), message.text().end(),
                     [&full_text] (auto const& word) { full_text.append(word); });
        std::string date = google::protobuf::util::TimeUtil::ToString(message.time());
        std::cout << desc->field(0)->name() << ":\t" << date << "\n"
                  << desc->field(1)->name() << ":\t" << message.from() << "\n"
                  << desc->field(2)->name() << ":\t" << full_text << "\nme: " << std::endl;
    }
}

class Server::ServiceImpl final : public chat::Chat::Service {
public:
    grpc::Status Stream(grpc::ServerContext* context,
        grpc::ServerReaderWriter<chat::Message, chat::Message>* stream) override;

    friend void Read(grpc::ServerReaderWriter<chat::Message, chat::Message>* stream);
    void Write(grpc::ServerReaderWriter<chat::Message, chat::Message>& stream);

private:
    static google::protobuf::Timestamp* GetDate();
private:
    chat::Message _response;
};

grpc::Status Server::ServiceImpl::Stream(grpc::ServerContext* context __attribute__((unused)),
                            grpc::ServerReaderWriter<chat::Message, chat::Message>* stream) {
    _response.set_from("simge");

    std::thread read(Read, stream);
    std::thread write([&] { this->Write(*stream); });

    write.join();
    read.join();
    return grpc::Status::OK;
}

google::protobuf::Timestamp* Server::ServiceImpl::GetDate() {
    google::protobuf::Timestamp* timestamp = new google::protobuf::Timestamp();
    struct timeval tv;
    gettimeofday(&tv, NULL);
    timestamp->set_seconds(tv.tv_sec);
    timestamp->set_nanos(tv.tv_usec * 1000);
    return timestamp;
}

void Server::ServiceImpl::Write(grpc::ServerReaderWriter<chat::Message, chat::Message>& stream) {
    std::string my_response;
    while (std::cout << "me: \n" && std::getline(std::cin, my_response)) {
        _response.clear_text(); _response.add_text(my_response);
        _response.set_allocated_time(ServiceImpl::GetDate());
        stream.Write(_response);
        my_response.clear();
    }
}

Server::Server() : _service(new ServiceImpl()) {
}

void Server::Run() {
    std::unique_ptr<grpc::ServerBuilder> builder(new grpc::ServerBuilder());
    builder->AddListeningPort("127.0.0.1:45677", grpc::InsecureServerCredentials());
    builder->RegisterService(_service.get());

    auto server = builder->BuildAndStart();
    server->Wait();
}

void Server::ShutDown() {
    _server->Shutdown();
}
} //namespace cross
int main(int argc __attribute__((unused)), char** argv __attribute__((unused))) {
    cross::Server server;
    server.Run();
    return 0;
}
