#include <memory>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <thread>
#include <grpcpp/grpcpp.h>
#include "chat.grpc.pb.h"

namespace cross {
class Client {
public:
    Client(std::shared_ptr<grpc::Channel> channel);
    ~Client() {};

    friend void Read(std::shared_ptr<grpc::ClientReaderWriter<chat::Message, chat::Message>> stream);

private:
     static google::protobuf::Timestamp* GetDate();
     void Write(std::shared_ptr<grpc::ClientReaderWriter<chat::Message, chat::Message>> stream);
private:
    Client() = delete;
    Client(Client const& /*client*/) = delete;
    Client& operator=(Client const& /*client*/) = delete;

private:
    std::unique_ptr<chat::Chat::Stub> _stub;
    grpc::ClientContext _context;
    chat::Message _response;
    std::shared_ptr<grpc::ClientReaderWriter<chat::Message, chat::Message>> _stream;
};
}