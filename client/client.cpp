#include "client.h"
#include <google/protobuf/util/time_util.h>

namespace cross {
void Read(std::shared_ptr<grpc::ClientReaderWriter<chat::Message, chat::Message>> stream) {
    cross::chat::Message message;
    while (stream->Read(&message)) {
        std::string full_text;
        google::protobuf::Descriptor const *desc = message.descriptor();
        std::for_each(message.text().begin(), message.text().end(),
                     [&full_text] (auto const& word) { full_text.append(word); });
        std::string date = google::protobuf::util::TimeUtil::ToString(message.time());
        std::cout << desc->field(0)->name() << ":\t" << date << "\n"
                  << desc->field(1)->name() << ":\t" << message.from() << "\n"
                  << desc->field(2)->name() << ":\t" << full_text << "\nme: " << std::endl;
    }
    grpc::Status status = stream->Finish();
}

Client::Client(std::shared_ptr<grpc::Channel> channel) 
       : _stub(std::move(chat::Chat::NewStub(channel)))
       , _stream(std::move(_stub->Stream(&_context))) {
    _response.set_from("bestie");

    std::thread read(Read, _stream);
    std::thread write([&] { this->Write(_stream); });

    write.join();
    read.join();
}

google::protobuf::Timestamp* Client::GetDate() {
    google::protobuf::Timestamp* timestamp = new google::protobuf::Timestamp();
    struct timeval tv;
    gettimeofday(&tv, NULL);
    timestamp->set_seconds(tv.tv_sec);
    timestamp->set_nanos(tv.tv_usec * 1000);
    return timestamp;
}

void Client::Write(std::shared_ptr<grpc::ClientReaderWriter<chat::Message, chat::Message>> stream) {
    std::string my_response;
    while (std::cout << "me: \n" && std::getline(std::cin, my_response)) {
        _response.clear_text(); _response.add_text(my_response);
        _response.set_allocated_time(Client::GetDate());
        stream->Write(_response);
        my_response.clear();
    }
}
} // namespace cross
int main(int argc __attribute__((unused)), char** argv __attribute__((unused))) {
    cross::Client client(grpc::CreateChannel("127.0.0.1:45677", grpc::InsecureChannelCredentials()));
    return 0;
}
